package chenpeng.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class JspFilter extends BasicFilter {

	
    @Override
	public void myFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
    	
    	HttpSession session=request.getSession();
    	String username=(String)session.getAttribute("username");//这里不能toString(),因为如果是空值的话，就会报空指针
    	String uri= request.getRequestURI();
    	System.out.println("进入jspFilter");
    	//上面拿到传的路径，下面判断是否是login.jsp结尾的
    	if (username!=null ||uri.endsWith("Login.jsp")) {
			chain.doFilter(request, response);
		}else{
			System.out.println("没有拿到username跳到登录页面");
			response.sendRedirect(request.getContextPath()+"/chenpeng/Login.jsp");
		}
	}
    
	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		chain.doFilter(request, response);
	}
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
