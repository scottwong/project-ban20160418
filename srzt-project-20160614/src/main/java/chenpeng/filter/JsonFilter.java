package chenpeng.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JsonFilter extends BasicFilter {

	public static void  printJson(ServletRequest req,HttpServletResponse response) throws IOException {

		
		HttpServletRequest request=(HttpServletRequest) req;
		PrintWriter out=response.getWriter();
		String jieguo  =request.getAttribute("jsonObjectScott").toString();
		out.write(jieguo);
		out.flush();
		out.close();
	}
	
	
	public void myFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		chain.doFilter(request, response);
		printJson(request, response);
	}
	
	
	
}
